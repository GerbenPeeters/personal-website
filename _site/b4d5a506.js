import{z as o}from"./c3bfbd24.js";
/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const{H:e}=o,n=o=>null===o||"object"!=typeof o&&"function"!=typeof o,t=(o,e)=>void 0===e?void 0!==(null==o?void 0:o._$litType$):(null==o?void 0:o._$litType$)===e,i=o=>void 0===o.strings,l=()=>document.createComment(""),A=(o,n,t)=>{var i;const A=o._$AA.parentNode,$=void 0===n?o._$AB:n._$AA;if(void 0===t){const n=A.insertBefore(l(),$),i=A.insertBefore(l(),$);t=new e(n,i,o,o.options)}else{const e=t._$AB.nextSibling,n=t._$AM,l=n!==o;if(l){let e;null===(i=t._$AQ)||void 0===i||i.call(t,o),t._$AM=o,void 0!==t._$AP&&(e=o._$AU)!==n._$AU&&t._$AP(e)}if(e!==$||l){let o=t._$AA;for(;o!==e;){const e=o.nextSibling;A.insertBefore(o,$),o=e}}}return t},$=(o,e,n=o)=>(o._$AI(e,n),o),_={},r=(o,e=_)=>o._$AH=e,s=o=>o._$AH,c=o=>{var e;null===(e=o._$AP)||void 0===e||e.call(o,!1,!0);let n=o._$AA;const t=o._$AB.nextSibling;for(;n!==t;){const o=n.nextSibling;n.remove(),n=o}};export{i as e,s as m,t as n,c as p,A as r,r as s,n as t,$ as u};
