import{_ as e}from"./161938fc.js";import{s as t,i as r}from"./e10f64e0.js";import{x as n,y as i}from"./c3bfbd24.js";import{o}from"./6eb35b51.js";import{e as s,i as u,t as a}from"./eb27e305.js";import{m as d,u as c,r as l,p as h,s as p}from"./b4d5a506.js";
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const f=(e,t,r)=>{const n=new Map;for(let i=t;i<=r;i++)n.set(e[i],i);return n},v=s(class extends u{constructor(e){if(super(e),e.type!==a.CHILD)throw Error("repeat() can only be used in text expressions")}ht(e,t,r){let n;void 0===r?r=t:void 0!==t&&(n=t);const i=[],o=[];let s=0;for(const t of e)i[s]=n?n(t,s):s,o[s]=r(t,s),s++;return{values:o,keys:i}}render(e,t,r){return this.ht(e,t,r).values}update(e,[t,r,i]){var o;const s=d(e),{values:u,keys:a}=this.ht(t,r,i);if(!Array.isArray(s))return this.ut=a,u;const v=null!==(o=this.ut)&&void 0!==o?o:this.ut=[],g=[];let m,y,_=0,x=s.length-1,b=0,w=u.length-1;for(;_<=x&&b<=w;)if(null===s[_])_++;else if(null===s[x])x--;else if(v[_]===a[b])g[b]=c(s[_],u[b]),_++,b++;else if(v[x]===a[w])g[w]=c(s[x],u[w]),x--,w--;else if(v[_]===a[w])g[w]=c(s[_],u[w]),l(e,g[w+1],s[_]),_++,w--;else if(v[x]===a[b])g[b]=c(s[x],u[b]),l(e,s[_],s[x]),x--,b++;else if(void 0===m&&(m=f(a,b,w),y=f(v,_,x)),m.has(v[_]))if(m.has(v[x])){const t=y.get(a[b]),r=void 0!==t?s[t]:null;if(null===r){const t=l(e,s[_]);c(t,u[b]),g[b]=t}else g[b]=c(r,u[b]),l(e,s[_],r),s[t]=null;b++}else h(s[x]),x--;else h(s[_]),_++;for(;b<=w;){const t=l(e,g[w+1]);c(t,u[b]),g[b++]=t}for(;_<=x;){const e=s[_++];null!==e&&h(e)}return this.ut=a,p(e,g),n}});
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */var g=function(){return g=Object.assign||function(e){for(var t,r=1,n=arguments.length;r<n;r++)for(var i in t=arguments[r])Object.prototype.hasOwnProperty.call(t,i)&&(e[i]=t[i]);return e},g.apply(this,arguments)};function m(e){var t="function"==typeof Symbol&&Symbol.iterator,r=t&&e[t],n=0;if(r)return r.call(e);if(e&&"number"==typeof e.length)return{next:function(){return e&&n>=e.length&&(e=void 0),{value:e&&e[n++],done:!e}}};throw new TypeError(t?"Object is not iterable.":"Symbol.iterator is not defined.")}function y(e,t){var r="function"==typeof Symbol&&e[Symbol.iterator];if(!r)return e;var n,i,o=r.call(e),s=[];try{for(;(void 0===t||t-- >0)&&!(n=o.next()).done;)s.push(n.value)}catch(e){i={error:e}}finally{try{n&&!n.done&&(r=o.return)&&r.call(o)}finally{if(i)throw i.error}}return s}function _(e,t,r){if(r||2===arguments.length)for(var n,i=0,o=t.length;i<o;i++)!n&&i in t||(n||(n=Array.prototype.slice.call(t,0,i)),n[i]=t[i]);return e.concat(n||Array.prototype.slice.call(t))}var x,b=function(){function e(e,t){var r=e._tree,n=Object.keys(r);this.set=e,this._type=t,this._path=n.length>0?[{node:r,keys:n}]:[]}return e.prototype.next=function(){var e=this.dive();return this.backtrack(),e},e.prototype.dive=function(){if(0===this._path.length)return{done:!0,value:void 0};var e=w(this._path),t=e.node,r=e.keys;return""===w(r)?{done:!1,value:this.result()}:(this._path.push({node:t[w(r)],keys:Object.keys(t[w(r)])}),this.dive())},e.prototype.backtrack=function(){0!==this._path.length&&(w(this._path).keys.pop(),w(this._path).keys.length>0||(this._path.pop(),this.backtrack()))},e.prototype.key=function(){return this.set._prefix+this._path.map((function(e){var t=e.keys;return w(t)})).filter((function(e){return""!==e})).join("")},e.prototype.value=function(){return w(this._path).node[""]},e.prototype.result=function(){return"VALUES"===this._type?this.value():"KEYS"===this._type?this.key():[this.key(),this.value()]},e.prototype[Symbol.iterator]=function(){return this},e}(),w=function(e){return e[e.length-1]},F=function(e,t,r,n,i,o){o.push({distance:0,ia:n,ib:0,edit:i});for(var s=[];o.length>0;){var u=o.pop(),a=u.distance,d=u.ia,c=u.ib,l=u.edit;if(c!==t.length)if(e[d]===t[c])o.push({distance:a,ia:d+1,ib:c+1,edit:0});else{if(a>=r)continue;2!==l&&o.push({distance:a+1,ia:d,ib:c+1,edit:3}),d<e.length&&(3!==l&&o.push({distance:a+1,ia:d+1,ib:c,edit:2}),3!==l&&2!==l&&o.push({distance:a+1,ia:d+1,ib:c+1,edit:1}))}else s.push({distance:a,i:d,edit:l})}return s},k=function(){function e(e,t){void 0===e&&(e={}),void 0===t&&(t=""),this._tree=e,this._prefix=t}return e.prototype.atPrefix=function(t){var r;if(!t.startsWith(this._prefix))throw new Error("Mismatched prefix");var n=y(E(this._tree,t.slice(this._prefix.length)),2),i=n[0],o=n[1];if(void 0===i){var s=y(D(o),2),u=s[0],a=s[1],d=Object.keys(u).find((function(e){return""!==e&&e.startsWith(a)}));if(void 0!==d)return new e(((r={})[d.slice(a.length)]=u[d],r),t)}return new e(i||{},t)},e.prototype.clear=function(){delete this._size,this._tree={}},e.prototype.delete=function(e){return delete this._size,j(this._tree,e)},e.prototype.entries=function(){return new b(this,"ENTRIES")},e.prototype.forEach=function(e){var t,r;try{for(var n=m(this),i=n.next();!i.done;i=n.next()){var o=y(i.value,2);e(o[0],o[1],this)}}catch(e){t={error:e}}finally{try{i&&!i.done&&(r=n.return)&&r.call(n)}finally{if(t)throw t.error}}},e.prototype.fuzzyGet=function(e,t){return function(e,t,r){for(var n=[{distance:0,i:0,key:"",node:e}],i={},o=[],s=function(){var e=n.pop(),s=e.node,u=e.distance,a=e.key,d=e.i,c=e.edit;Object.keys(s).forEach((function(e){if(""===e){var l=u+(t.length-d),h=y(i[a]||[null,1/0],2)[1];l<=r&&l<h&&(i[a]=[s[e],l])}else F(t,e,r-u,d,c,o).forEach((function(t){var r=t.distance,i=t.i,o=t.edit;n.push({node:s[e],distance:u+r,key:a+e,i:i,edit:o})}))}))};n.length>0;)s();return i}(this._tree,e,t)},e.prototype.get=function(e){var t=A(this._tree,e);return void 0!==t?t[""]:void 0},e.prototype.has=function(e){var t=A(this._tree,e);return void 0!==t&&t.hasOwnProperty("")},e.prototype.keys=function(){return new b(this,"KEYS")},e.prototype.set=function(e,t){if("string"!=typeof e)throw new Error("key must be a string");return delete this._size,I(this._tree,e)[""]=t,this},Object.defineProperty(e.prototype,"size",{get:function(){var e=this;return this._size||(this._size=0,this.forEach((function(){e._size+=1}))),this._size},enumerable:!1,configurable:!0}),e.prototype.update=function(e,t){if("string"!=typeof e)throw new Error("key must be a string");delete this._size;var r=I(this._tree,e);return r[""]=t(r[""]),this},e.prototype.values=function(){return new b(this,"VALUES")},e.prototype[Symbol.iterator]=function(){return this.entries()},e.from=function(t){var r,n,i=new e;try{for(var o=m(t),s=o.next();!s.done;s=o.next()){var u=y(s.value,2),a=u[0],d=u[1];i.set(a,d)}}catch(e){r={error:e}}finally{try{s&&!s.done&&(n=o.return)&&n.call(o)}finally{if(r)throw r.error}}return i},e.fromObject=function(t){return e.from(Object.entries(t))},e}(),E=function(e,t,r){if(void 0===r&&(r=[]),0===t.length||null==e)return[e,r];var n=Object.keys(e).find((function(e){return""!==e&&t.startsWith(e)}));return void 0===n?(r.push([e,t]),E(void 0,"",r)):(r.push([e,n]),E(e[n],t.slice(n.length),r))},A=function(e,t){if(0===t.length||null==e)return e;var r=Object.keys(e).find((function(e){return""!==e&&t.startsWith(e)}));return void 0!==r?A(e[r],t.slice(r.length)):void 0},I=function(e,t){var r;if(0===t.length||null==e)return e;var n=Object.keys(e).find((function(e){return""!==e&&t.startsWith(e)}));if(void 0===n){var i=Object.keys(e).find((function(e){return""!==e&&e.startsWith(t[0])}));if(void 0!==i){var o=z(t,i);return e[o]=((r={})[i.slice(o.length)]=e[i],r),delete e[i],I(e[o],t.slice(o.length))}return e[t]={},e[t]}return I(e[n],t.slice(n.length))},z=function(e,t,r,n,i){return void 0===r&&(r=0),void 0===n&&(n=Math.min(e.length,t.length)),void 0===i&&(i=""),r>=n||e[r]!==t[r]?i:z(e,t,r+1,n,i+e[r])},j=function(e,t){var r=y(E(e,t),2),n=r[0],i=r[1];if(void 0!==n){delete n[""];var o=Object.keys(n);0===o.length&&C(i),1===o.length&&S(i,o[0],n[o[0]])}},C=function(e){if(0!==e.length){var t=y(D(e),2),r=t[0];delete r[t[1]];var n=Object.keys(r);0===n.length&&C(e.slice(0,-1)),1===n.length&&""!==n[0]&&S(e.slice(0,-1),n[0],r[n[0]])}},S=function(e,t,r){if(0!==e.length){var n=y(D(e),2),i=n[0],o=n[1];i[o+t]=r,delete i[o]}},D=function(e){return e[e.length-1]},O=function(){function e(e){if(null==(null==e?void 0:e.fields))throw new Error('MiniSearch: option "fields" must be provided');this._options=g(g(g({},$),e),{searchOptions:g(g({},J),e.searchOptions||{})}),this._index=new k,this._documentCount=0,this._documentIds={},this._fieldIds={},this._fieldLength={},this._averageFieldLength={},this._nextId=0,this._storedFields={},this.addFields(this._options.fields)}return e.prototype.add=function(e){var t=this,r=this._options,n=r.extractField,i=r.tokenize,o=r.processTerm,s=r.fields,u=r.idField,a=n(e,u);if(null==a)throw new Error('MiniSearch: document does not have ID field "'.concat(u,'"'));var d=this.addDocumentId(a);this.saveStoredFields(d,e),s.forEach((function(r){var s=n(e,r);if(null!=s){var u=i(s.toString(),r);t.addFieldLength(d,t._fieldIds[r],t.documentCount-1,u.length),u.forEach((function(e){var n=o(e,r);n&&t.addTerm(t._fieldIds[r],d,n)}))}}))},e.prototype.addAll=function(e){var t=this;e.forEach((function(e){return t.add(e)}))},e.prototype.addAllAsync=function(e,t){var r=this;void 0===t&&(t={});var n=t.chunkSize,i=void 0===n?10:n,o={chunk:[],promise:Promise.resolve()},s=e.reduce((function(e,t,n){var o=e.chunk,s=e.promise;return o.push(t),(n+1)%i==0?{chunk:[],promise:s.then((function(){return new Promise((function(e){return setTimeout(e,0)}))})).then((function(){return r.addAll(o)}))}:{chunk:o,promise:s}}),o),u=s.chunk;return s.promise.then((function(){return r.addAll(u)}))},e.prototype.remove=function(e){var t=this,r=this._options,n=r.tokenize,i=r.processTerm,o=r.extractField,s=r.fields,u=r.idField,a=o(e,u);if(null==a)throw new Error('MiniSearch: document does not have ID field "'.concat(u,'"'));var d=y(Object.entries(this._documentIds).find((function(e){var t=y(e,2);t[0];var r=t[1];return a===r}))||[],1),c=d[0];if(null==c)throw new Error("MiniSearch: cannot remove document with ID ".concat(a,": it is not in the index"));s.forEach((function(r){var s=o(e,r);if(null!=s){var u=n(s.toString(),r);u.forEach((function(e){var n=i(e,r);n&&t.removeTerm(t._fieldIds[r],c,n)})),t.removeFieldLength(c,t._fieldIds[r],t.documentCount,u.length)}})),delete this._storedFields[c],delete this._documentIds[c],delete this._fieldLength[c],this._documentCount-=1},e.prototype.removeAll=function(e){var t=this;if(e)e.forEach((function(e){return t.remove(e)}));else{if(arguments.length>0)throw new Error("Expected documents to be present. Omit the argument to remove all documents.");this._index=new k,this._documentCount=0,this._documentIds={},this._fieldLength={},this._averageFieldLength={},this._storedFields={},this._nextId=0}},e.prototype.search=function(e,t){var r=this;void 0===t&&(t={});var n=this.executeQuery(e,t);return Object.entries(n).reduce((function(e,n){var i=y(n,2),o=i[0],s=i[1],u=s.score,a=s.match,d=s.terms,c={id:r._documentIds[o],terms:M(d),score:u,match:a};return Object.assign(c,r._storedFields[o]),(null==t.filter||t.filter(c))&&e.push(c),e}),[]).sort((function(e,t){return e.score<t.score?1:-1}))},e.prototype.autoSuggest=function(e,t){void 0===t&&(t={}),t=g(g({},U),t);var r=this.search(e,t).reduce((function(e,t){var r=t.score,n=t.terms,i=n.join(" ");return null==e[i]?e[i]={score:r,terms:n,count:1}:(e[i].score+=r,e[i].count+=1),e}),{});return Object.entries(r).map((function(e){var t=y(e,2),r=t[0],n=t[1],i=n.score;return{suggestion:r,terms:n.terms,score:i/n.count}})).sort((function(e,t){return e.score<t.score?1:-1}))},Object.defineProperty(e.prototype,"documentCount",{get:function(){return this._documentCount},enumerable:!1,configurable:!0}),e.loadJSON=function(t,r){if(null==r)throw new Error("MiniSearch: loadJSON should be given the same options used when serializing the index");return e.loadJS(JSON.parse(t),r)},e.getDefault=function(e){if($.hasOwnProperty(e))return L($,e);throw new Error('MiniSearch: unknown option "'.concat(e,'"'))},e.loadJS=function(t,r){var n=t.index,i=t.documentCount,o=t.nextId,s=t.documentIds,u=t.fieldIds,a=t.fieldLength,d=t.averageFieldLength,c=t.storedFields,l=new e(r);return l._index=new k(n._tree,n._prefix),l._documentCount=i,l._nextId=o,l._documentIds=s,l._fieldIds=u,l._fieldLength=a,l._averageFieldLength=d,l._fieldIds=u,l._storedFields=c||{},l},e.prototype.executeQuery=function(e,t){var r=this;if(void 0===t&&(t={}),"string"==typeof e)return this.executeSearch(e,t);var n=e.queries.map((function(n){var i=g(g(g({},t),e),{queries:void 0});return r.executeQuery(n,i)}));return this.combineResults(n,e.combineWith)},e.prototype.executeSearch=function(e,t){var r=this;void 0===t&&(t={});var n=this._options,i=n.tokenize,o=n.processTerm,s=n.searchOptions,u=g(g({tokenize:i,processTerm:o},s),t),a=u.tokenize,d=u.processTerm,c=a(e).map((function(e){return d(e)})).filter((function(e){return!!e})).map(T(u)).map((function(e){return r.executeQuerySpec(e,u)}));return this.combineResults(c,u.combineWith)},e.prototype.executeQuerySpec=function(e,t){var r=this,n=g(g({},this._options.searchOptions),t),i=(n.fields||this._options.fields).reduce((function(e,t){var r;return g(g({},e),((r={})[t]=L(e,t)||1,r))}),n.boost||{}),o=n.boostDocument,s=n.weights,u=n.maxFuzzy,a=g(g({},J.weights),s),d=a.fuzzy,c=a.prefix,l=this.termResults(e.term,i,o,this._index.get(e.term));if(!e.fuzzy&&!e.prefix)return l;var h=[l];if(e.prefix&&this._index.atPrefix(e.term).forEach((function(t,n){var s=.3*(t.length-e.term.length)/t.length;h.push(r.termResults(t,i,o,n,c,s))})),e.fuzzy){var p=!0===e.fuzzy?.2:e.fuzzy,f=p<1?Math.min(u,Math.round(e.term.length*p)):p;Object.entries(this._index.fuzzyGet(e.term,f)).forEach((function(e){var t=y(e,2),n=t[0],s=y(t[1],2),u=s[0],a=s[1]/n.length;h.push(r.termResults(n,i,o,u,d,a))}))}return h.reduce(B.or)},e.prototype.combineResults=function(e,t){if(void 0===t&&(t="or"),0===e.length)return{};var r=t.toLowerCase();return e.reduce(B[r])||{}},e.prototype.toJSON=function(){return{index:this._index,documentCount:this._documentCount,nextId:this._nextId,documentIds:this._documentIds,fieldIds:this._fieldIds,fieldLength:this._fieldLength,averageFieldLength:this._averageFieldLength,storedFields:this._storedFields}},e.prototype.termResults=function(e,t,r,n,i,o){var s=this;return void 0===o&&(o=0),null==n?{}:Object.entries(t).reduce((function(t,i){var u=y(i,2),a=u[0],d=u[1],c=s._fieldIds[a],l=n[c]||{ds:{}},h=l.df,p=l.ds;return Object.entries(p).forEach((function(n){var i=y(n,2),u=i[0],l=i[1],p=r?r(s._documentIds[u],e):1;if(p){var f=s._fieldLength[u][c]/s._averageFieldLength[c];t[u]=t[u]||{score:0,match:{},terms:[]},t[u].terms.push(e),t[u].match[e]=L(t[u].match,e)||[],t[u].score+=p*R(l,h,s._documentCount,f,d,o),t[u].match[e].push(a)}})),t}),{})},e.prototype.addTerm=function(e,t,r){this._index.update(r,(function(r){var n,i=(r=r||{})[e]||{df:0,ds:{}};return null==i.ds[t]&&(i.df+=1),i.ds[t]=(i.ds[t]||0)+1,g(g({},r),((n={})[e]=i,n))}))},e.prototype.removeTerm=function(e,t,r){var n=this;this._index.has(r)?(this._index.update(r,(function(i){var o,s=i[e];if(null==s||null==s.ds[t])return n.warnDocumentChanged(t,e,r),i;if(s.ds[t]<=1){if(s.df<=1)return delete i[e],i;s.df-=1}return s.ds[t]<=1?(delete s.ds[t],i):(s.ds[t]-=1,g(g({},i),((o={})[e]=s,o)))})),0===Object.keys(this._index.get(r)).length&&this._index.delete(r)):this.warnDocumentChanged(t,e,r)},e.prototype.warnDocumentChanged=function(e,t,r){if(null!=console&&null!=console.warn){var n=Object.entries(this._fieldIds).find((function(e){var r=y(e,2);return r[0],r[1]===t}))[0];console.warn("MiniSearch: document with ID ".concat(this._documentIds[e],' has changed before removal: term "').concat(r,'" was not present in field "').concat(n,'". Removing a document after it has changed can corrupt the index!'))}},e.prototype.addDocumentId=function(e){var t=this._nextId.toString(36);return this._documentIds[t]=e,this._documentCount+=1,this._nextId+=1,t},e.prototype.addFields=function(e){var t=this;e.forEach((function(e,r){t._fieldIds[e]=r}))},e.prototype.addFieldLength=function(e,t,r,n){this._averageFieldLength[t]=this._averageFieldLength[t]||0;var i=this._averageFieldLength[t]*r+n;this._fieldLength[e]=this._fieldLength[e]||{},this._fieldLength[e][t]=n,this._averageFieldLength[t]=i/(r+1)},e.prototype.removeFieldLength=function(e,t,r,n){var i=this._averageFieldLength[t]*r-n;this._averageFieldLength[t]=i/(r-1)},e.prototype.saveStoredFields=function(e,t){var r=this,n=this._options,i=n.storeFields,o=n.extractField;null!=i&&0!==i.length&&(this._storedFields[e]=this._storedFields[e]||{},i.forEach((function(n){var i=o(t,n);void 0!==i&&(r._storedFields[e][n]=i)})))},e}(),L=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)?e[t]:void 0},B=((x={}).or=function(e,t){return Object.entries(t).reduce((function(e,t){var r,n=y(t,2),i=n[0],o=n[1],s=o.score,u=o.match,a=o.terms;return null==e[i]?e[i]={score:s,match:u,terms:a}:(e[i].score+=s,e[i].score*=1.5,(r=e[i].terms).push.apply(r,_([],y(a),!1)),Object.assign(e[i].match,u)),e}),e||{})},x.and=function(e,t){return Object.entries(t).reduce((function(t,r){var n=y(r,2),i=n[0],o=n[1],s=o.score,u=o.match,a=o.terms;return void 0===e[i]||(t[i]=t[i]||{},t[i].score=e[i].score+s,t[i].match=g(g({},e[i].match),u),t[i].terms=_(_([],y(e[i].terms),!1),y(a),!1)),t}),{})},x.and_not=function(e,t){return Object.entries(t).reduce((function(e,t){var r=y(t,2),n=r[0],i=r[1];return i.score,i.match,i.terms,delete e[n],e}),e||{})},x),R=function(e,t,r,n,i,o){var s,u;return i/(1+.333*i*o)*(s=t,u=r,e*Math.log(u/s))/n},T=function(e){return function(t,r,n){return{term:t,fuzzy:"function"==typeof e.fuzzy?e.fuzzy(t,r,n):e.fuzzy||!1,prefix:"function"==typeof e.prefix?e.prefix(t,r,n):!0===e.prefix}}},M=function(e){return e.filter((function(e,t,r){return r.indexOf(e)===t}))},$={idField:"id",extractField:function(e,t){return e[t]},tokenize:function(e,t){return e.split(P)},processTerm:function(e,t){return e.toLowerCase()},fields:void 0,searchOptions:void 0,storeFields:[]},J={combineWith:"or",prefix:!1,fuzzy:!1,maxFuzzy:6,boost:{},weights:{fuzzy:.9,prefix:.75}},U={prefix:function(e,t,r){return t===r.length-1}},P=/[\n\r -#%-*,-/:;?@[-\]_{}\u00A0\u00A1\u00A7\u00AB\u00B6\u00B7\u00BB\u00BF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0A76\u0AF0\u0C77\u0C84\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166E\u1680\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2000-\u200A\u2010-\u2029\u202F-\u2043\u2045-\u2051\u2053-\u205F\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E4F\u3000-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]+/u;function W(e,t){return e===t?e:`${e} > ${t}`}function N(e){return`<strong>${e}</strong>`}function q({search:e,text:t,terms:r,before:n=15,length:i=100,highlight:o=N,addEllipsis:s=!1}){if(!e||!t)return"";let u,a=t,d=a.toLowerCase(),c=0,l=0;for(const t of r){let r=0,s=0;const h=t.substring(0,e.length)===e?e.length:t.length;do{if(s=d.indexOf(t,r),-1!==s){const e=s+h,t=o(a.slice(s,e));a=[a.slice(0,s),t,a.slice(e)].join(""),d=a.toLowerCase(),r=s+t.length,(void 0===u||s<u)&&(u=s,l=u-n>0?u-n:0),s-l-c<i&&(c+=t.length-h)}}while(-1!==s)}let h=a.substr(l,i+c);return s&&l>0&&(h=`...${h}`),h}const K=e=>Promise.allSettled(e.getAnimations().map((e=>e.finished)));class Q extends t{constructor(){super(),this.open=!1,this.jsonUrl="",this.search="",this.maxResults=10,this.selectedIndex=-1,this.noResultsText="No results found",this.__isSetup=!1,this.results=[],this.miniSearch=null}async setupSearch(){if(this.__isSetup)return;if(!this.jsonUrl)throw new Error('You need to provide a URL to your JSON index. For example: <rocket-search json-url="https://.../search-index.json"></rocket-search>');let e;try{const t=await fetch(this.jsonUrl);e=await t.text()}catch(e){throw new Error(`The given json-url "${this.jsonUrl}" could not be fetched.`)}if("{"!==e[0])throw new Error(`The given json-url "${this.jsonUrl}" could not be fetched.`);this.miniSearch=O.loadJSON(e,{fields:["title","headline","body"],searchOptions:{boost:{headline:3,title:2},fuzzy:.2,prefix:!0}}),this.__isSetup=!0}render(){return i`
      <slot name="invoker">
        <div class="input-group__container">
          <div class="input-group__prefix">
            <label for="search-input" id="search-label">
              <span class="sr-only">Search:</span>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129">
                <path
                  d="M51.6 96.7c11 0 21-3.9 28.8-10.5l35 35c.8.8 1.8 1.2 2.9 1.2s2.1-.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-35-35c6.5-7.8 10.5-17.9 10.5-28.8 0-24.9-20.2-45.1-45.1-45.1-24.8 0-45.1 20.3-45.1 45.1 0 24.9 20.3 45.1 45.1 45.1zm0-82c20.4 0 36.9 16.6 36.9 36.9C88.5 72 72 88.5 51.6 88.5S14.7 71.9 14.7 51.6c0-20.3 16.6-36.9 36.9-36.9z"
                />
              </svg>
            </label>
          </div>
          <div class="input-group__input">
            <input
              type="text"
              id="search-input"
              @input=${this._onInput}
              @focusin=${this.setupSearch}
              @keydown=${this._onKeyDown}
            />
          </div>
        </div>
      </slot>
      <dialog @close=${this.close}>
        <div id="close">
          <slot name="close">
            <button class="close" aria-label="close" @click=${this.close}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 460.775 460.775">
                <path
                  d="M285.08 230.397 456.218 59.27c6.076-6.077 6.076-15.911 0-21.986L423.511 4.565a15.55 15.55 0 0 0-21.985 0l-171.138 171.14L59.25 4.565a15.551 15.551 0 0 0-21.985 0L4.558 37.284c-6.077 6.075-6.077 15.909 0 21.986l171.138 171.128L4.575 401.505c-6.074 6.077-6.074 15.911 0 21.986l32.709 32.719a15.555 15.555 0 0 0 21.986 0l171.117-171.12 171.118 171.12a15.551 15.551 0 0 0 21.985 0l32.709-32.719c6.074-6.075 6.074-15.909 0-21.986L285.08 230.397z"
                />
              </svg>
            </button>
          </slot>
        </div>
        <div id="content">
          ${v(this.results,(e=>e.id),((e,t)=>i`
              <a
                href="${e.id}"
                rel="noopener noreferrer"
                class="result"
                ?selected=${this.selectedIndex===t}
              >
                <span class="result__title"
                  >${o(function({result:e,search:t}){const{terms:r,title:n,headline:i}=e;return q({text:W(n,i),search:t,terms:r})}({result:e,search:this.search}))}</span
                >
                <span class="result__text"
                  >${o(function({result:e,search:t}){const{terms:r,body:n}=e;return q({text:n,search:t,terms:r,addEllipsis:!0})}({result:e,search:this.search}))}</span
                >
              </a>
            `))}
        </div>
      </dialog>
    `}_onInput(e){this.search=e.target.value,this.selectedIndex=-1}_onKeyDown(e){switch(e.code){case"Escape":this.close();break;case"ArrowDown":this.selectedIndex=this.selectedIndex<this.results.length-1?this.selectedIndex+1:0,e.preventDefault();break;case"ArrowUp":this.selectedIndex=this.selectedIndex>0?this.selectedIndex-1:this.results.length-1,e.preventDefault();break;case"Enter":this.selectedIndex>=0&&(document.location=this.results[this.selectedIndex].id)}}close(){this.open=!1}show(){this.open=!0}updated(e){super.updated(e),e.has("open")&&(this.open?this.showModal():this._close())}update(e){this.miniSearch&&e.has("search")&&(this.search.length>1?(this.results=this.miniSearch.search(this.search).slice(0,this.maxResults),this.results.length>0&&(this.open=!0)):this.open=!1),super.update(e)}alignDialogToInvoker(){if(!this._dialog)return;const e=this.getBoundingClientRect();let t=e.left+e.width/2-this._dialog.clientWidth/2-10;t<0&&(t=10),this._dialog.style.marginTop=`${e.y+e.height+5}px`,window.innerWidth>=768&&(this._dialog.style.marginLeft=t+"px")}firstUpdated(e){super.firstUpdated(e),this.shadowRoot&&(this._dialog=this.shadowRoot.querySelector("dialog"),this._searchInput=this.shadowRoot.querySelector("input"))}async _close(){this._dialog&&(this._dialog.setAttribute("inert",""),this._dialog.dispatchEvent(new Event("closing")),await K(this._dialog),this._dialog.dispatchEvent(new Event("closed")),this._dialog.close())}async showModal(){this._dialog&&this._searchInput&&(this.alignDialogToInvoker(),this._dialog.show(),this._dialog.dispatchEvent(new Event("opening")),this._searchInput.focus(),await K(this._dialog),this._dialog.dispatchEvent(new Event("opened")),this._dialog.removeAttribute("inert"))}async toggle(){this.open=!this.open}focus(){this._searchInput&&this._searchInput.focus()}}e(Q,"properties",{open:{type:Boolean,reflect:!0},jsonUrl:{type:String,attribute:"json-url"},search:{type:String},results:{type:Array},maxResults:{type:Number,attribute:"max-results"},noResultsText:{type:String},selectedIndex:{type:Number}}),e(Q,"styles",[r`
      :host {
        display: block;
        --rd-surface-1: var(--surface-1, #f8f9fa);
        --rd-surface-2: var(--surface-2, #e9ecef);
        --rd-surface-3: var(--surface-3, #dee2e6);
        --rd-text-1: var(--text-1, #212529);
        /* shadow */
        --rd-shadow-color: 220 3% 15%;
        --rd-shadow-strength: 1%;
        --rd-shadow-2: 0 3px 5px -2px hsl(var(--rd-shadow-color) /
                calc(var(--rd-shadow-strength) + 3%)),
          0 7px 14px -5px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 5%));
        --rd-shadow-6: 0 -1px 2px 0 hsl(var(--rd-shadow-color) /
                calc(var(--rd-shadow-strength) + 2%)),
          0 3px 2px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 3%)),
          0 7px 5px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 3%)),
          0 12px 10px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 4%)),
          0 22px 18px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 5%)),
          0 41px 33px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 6%)),
          0 100px 80px -2px hsl(var(--rd-shadow-color) / calc(var(--rd-shadow-strength) + 7%));
        /* size */
        --rd-size-3: var(--size-3, 1rem);
        --rd-size-5: var(--size-5, 1.5rem);
        /* animations */
        --rd-ease-3: cubic-bezier(0.25, 0, 0.3, 1);
        --rd-animation-slide-out-down: var(
          --animation-slide-out-down,
          slide-out-down 0.5s var(--rd-ease-3)
        );
        --rd-animation-slide-in-up: var(--animation-slide-in-up, slide-in-up 0.5s var(--rd-ease-3));
      }

      #close {
        position: absolute;
        right: 0;
        top: 0;
      }

      button {
        background: none;
        border: none;
        width: 30px;
        fill: var(--rd-text-1);
        padding: 5px;
        margin: 5px;
        display: block;
      }

      button svg {
        display: block;
      }

      /*****************************
      * input
      *****************************/
      #search-input {
        display: block;
        padding: 0;
        margin-right: 15px;
        font-size: 22px;
        border: none;
        outline: none;
        width: 15ch;
      }

      .input-group__container {
        position: relative;
        background: var(--rocket-search-background-color, #fff);
        display: flex;
        border: 1px solid var(--rocket-search-input-border-color, #dfe1e5);
        box-shadow: none;
        border-radius: var(--rocket-search-input-border-radius, 24px);
        padding: 5px 0;
      }

      .input-group {
        margin-bottom: 16px;
        max-width: 582px;
        margin: auto;
        font-size: 20px;
        line-height: 20px;
      }

      .input-group__prefix,
      .input-group__suffix {
        display: flex;
        place-items: center;
        padding: 0 10px 0 15px;
      }

      .input-group__input {
        display: flex;
        place-items: center;
      }

      #search-label svg {
        width: 20px;
        height: 20px;
        display: block;
      }

      /** no way to override https://twitter.com/daKmoR/status/1528305586466697216?s=20&t=wBoqkZc3jfjFIJZJd21p_g
      @media (prefers-color-scheme: dark) {
        :host {
          --rd-surface-1: var(--surface-1, #212529);
          --rd-surface-2: var(--surface-2, #343a40);
          --rd-surface-3: var(--surface-3, #495057);
          --rd-text-1: var(--text-1, #f1f3f5);
          --rd-shadow-color: 220 40% 2%;
          --rd-shadow-strength: 25%;
        }
      }
      */

      dialog {
        display: block;
        background: var(--rd-surface-2);
        color: var(--rd-text-1);
        width: 100vw;
        max-height: 100vh;
        margin: 0;
        padding: 0;
        position: fixed;
        inset: 0;
        border: none;
        box-shadow: var(--rd-shadow-6);
        z-index: 2147483647;
        overflow: hidden;
        transition: opacity 0.5s var(--rd-ease-3);
      }

      @media screen and (min-width: 1024px) {
        dialog {
          width: min(80vw, 60ch);
        }
      }

      @media (prefers-reduced-motion: no-preference) {
        dialog {
          animation: var(--rd-animation-slide-out-down) forwards;
          animation-timing-function: var(--ease-squish-3);
        }
      }

      /** no way to override https://twitter.com/daKmoR/status/1528305586466697216?s=20&t=wBoqkZc3jfjFIJZJd21p_g
      @media (prefers-color-scheme: dark) {
        dialog {
          -webkit-border-before: 1px solid var(--_rg-surface-3);
          border-block-start: 1px solid var(--_rg-surface-3);
        }
      }
      */

      dialog:not([open]) {
        pointer-events: none;
        opacity: 0;
      }

      dialog::-webkit-backdrop {
        -webkit-backdrop-filter: none;
        backdrop-filter: none;
        -webkit-transition: -webkit-backdrop-filter 0.5s ease;
        transition: -webkit-backdrop-filter 0.5s ease;
        transition: backdrop-filter 0.5s ease;
        transition: backdrop-filter 0.5s ease, -webkit-backdrop-filter 0.5s ease;
      }

      dialog::backdrop {
        -webkit-backdrop-filter: none;
        backdrop-filter: none;
        transition: -webkit-backdrop-filter 0.5s ease;
        transition: backdrop-filter 0.5s ease;
        transition: backdrop-filter 0.5s ease, -webkit-backdrop-filter 0.5s ease;
      }

      @media (prefers-reduced-motion: no-preference) {
        dialog[open] {
          animation: var(--rd-animation-slide-in-up) forwards;
        }
      }

      @keyframes slide-in-up {
        from {
          transform: translateY(100%);
        }
      }

      @keyframes slide-out-down {
        to {
          transform: translateY(100%);
        }
      }

      .sr-only {
        position: absolute;
        left: -10000px;
        top: auto;
        width: 1px;
        height: 1px;
        overflow: hidden;
      }

      .result {
        display: block;
        color: var(--rd-text-1);
        padding: 10px 10px 9px 10px;
        text-decoration: none;
        border: 1px solid transparent;
        border-bottom-color: #ccc;
      }

      .result:last-child {
        border-bottom-color: transparent;
      }

      .result__title {
        display: block;
        font-size: 1.15rem;
        padding-bottom: 3px;
      }

      .result__text {
        font-size: 0.85rem;
      }

      .result[selected] {
        border-color: blue;
      }
    `]);export{Q as RocketSearch};
